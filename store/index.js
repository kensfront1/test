import Vue from 'vue'
import Vuex from 'vuex'
import editUser from './modules/editUser'
import modal from './modules/modal'
Vue.use(Vuex)


export default ()=> new Vuex.Store({
    modules:{
        editUser,
        modal
    }
})