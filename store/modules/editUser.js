export default  {
    actions:{
        addingUser(ctx,user){
            ctx.commit('addUser',user)
        },
        deletionUser(ctx,{id}){
            ctx.commit('deleteUser',id)
        },
        searchingUser(ctx,obj){
            ctx.commit('searchUser',obj)
        },
        editingUser(ctx,user){
            ctx.commit('editUser',user)
        },
        checkingAllRow(ctx,{isChecked}){
            ctx.commit('checkedAllRow',isChecked)
        },
        deletionCheckedRow(ctx){
            ctx.commit('deleteCheckedRow')
        }
    },
    mutations:{
        addUser(state,user){
            if(user.name!==''){
                state.users.push(user)
                state.currentUsers=[...state.users]
            }
            else{
                alert('Введите пользователя')
            }
        },
        deleteUser(state,id){
            state.users=state.users.filter(user=>{
                return user.id!==id
            })
            state.currentUsers=[...state.users]
        },
        editUser(state,user){
            const idx=state.users.findIndex(item=>{
                return item.id==user.id
            })
            state.currentUsers[idx].name=user.name
            state.currentUsers=[...state.users]
        },
        searchUser(state,obj){
            state.currentUsers=state.users.filter(user=>{
                return user.name.toLowerCase().indexOf(obj.str.toLowerCase())>-1
            })
        },
        checkedAllRow(state,isChecked){
            if(isChecked){
                state.currentUsers.forEach(user=>{
                    user.checked=true
                    state.allChecked=true
                })
            }
            else{
                state.currentUsers.forEach(user=>{
                    user.checked=false
                    state.allChecked=false
                })                
            }
        },
        deleteCheckedRow(state){
            state.users=state.users.filter(user=>{
                return user.checked==false
            })  
            state.allChecked=false
            state.currentUsers=[...state.users]
        }
    },
    state:{
        users:[],
        currentUsers:[],
        allChecked:false
    },
    getters:{
        returnUsers(state){
            return state.users
        },
        returnCurrentUsers(state){
            return state.currentUsers
        },
        returnCountUsers(state){
            return state.users.length
        },
        returnAllChecked(state){
            return state.allChecked
        }
    },
}