export default  {
    actions:{
        toOpeningModal(ctx){
            ctx.commit('toOpenModal')
        },
        toClosingModal(ctx){
            ctx.commit('toCloseModal')
        }        
    },
    mutations:{
        toOpenModal(state){
            state.isModal=true
        },
        toCloseModal(state){
            state.isModal=false
        }
    },
    state:{
        isModal:false
    },
    getters:{
        returnIsModal(state){
            return state.isModal
        }
    },
}